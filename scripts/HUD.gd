extends CanvasLayer

var tasks = [["Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread"],
			 ["Cavern Bread","Dankshroom","Tinderbox Morel","Luminous Lichen"],
			 ["Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Funeral Knell"],
			 ["Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Luminous Lichen","Luminous Lichen","Luminous Lichen","Tinderbox Morel","Tinderbox Morel","Tinderbox Morel"],
			 ["Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Cavern Bread","Funeral Knell","Luminous Lichen","Luminous Lichen","Tinderbox Morel","Tinderbox Morel","Luminous Lichen","Luminous Lichen"],
	]
var harvested = []

var day_length = 180
var fertiliser = 6

signal start_game
signal end_game

var started = false

func _ready():
	$book.hide()
	#Start the game!
	emit_signal("start_game")
	$gametime.start()
	$fertiliser_count.text = str(fertiliser)

func _on_gametime_timeout():
	#Do the count down
	day_length += -1
	$time.text = "Time Left: " + str(day_length)
	if day_length == 0:
		$GameFailSound.play()
		get_tree().change_scene("res://Fail.tscn")
func _on_farm_fertiliser_update(num):
	#Update fert numbers
	fertiliser += num
	$fertiliser_count.text = str(fertiliser)

func _on_farm_temp_update(num):
	$heattrack.frame = num
	
func _on_farm_light_update(num):
	$lighttrack.frame = num

func _on_farm_moist_update(num):
	$wettrack.frame = num

func _on_OrderButton_pressed():
	var tasklist = ""
	for i in tasks[0]:
		tasklist += i + "\n"
	$tasklist/Label.text = tasklist
	$tasklist.popup()

func _on_farm_harvert_list_update(input):
	harvested.append(input)
	print(harvested)
	task_update(input)

func task_update(shroomtype):
	var search = tasks[0].find(shroomtype)
	if search != -1:
		tasks[0].remove(search)
	print(tasks[0])
	if tasks[0].size() == 0:
		print("Task Complete! Put stuff here for completed task!")
		$TaskCompleteSound.play()
		tasks.remove(0)
		day_length = 180
	if tasks.size() == 1:
		$tasklist/title.text = "Button's Shopping List"
	if tasks.size() == 0:
		print("GAME COMPLETE")
		get_tree().change_scene("res://scripts/Success.tscn")


func _on_ShroomButton_pressed():
	if $book.visible == true:
		$book.visible = false
	elif $book.visible == false:
		$book.visible = true
