extends KinematicBody2D

var speed = 400
var past_coords = Vector2 (0,0)
var paused

var up
var down
var left
var right

var velocity = Vector2()

func get_input():
	#Handle movement
	if paused:
		$AnimatedSprite.animation = "idle"
	if !paused:
		up = false
		down = false
		right = false
		left = false
		
		velocity = Vector2()
		
		if Input.is_action_pressed("ui_right"):
			velocity.x += 10
			right = true
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 10
			left = true
		if Input.is_action_pressed("ui_down"):
			velocity.y += 10
			down = true
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 10
			up = true
		if velocity == Vector2(0,0):
			$AnimatedSprite.animation = "idle"
			
		if up and right:
			$AnimatedSprite.animation = "run_back"
		elif up and left:
			$AnimatedSprite.animation = "run_back_flip"
		elif down and right:
			$AnimatedSprite.animation = "run_front_flip"
		elif down and left:
			$AnimatedSprite.animation = "run_front"
		elif up:
			$AnimatedSprite.animation = "run_back"
		elif down:
			$AnimatedSprite.animation = "run_front"
		elif left:
			$AnimatedSprite.animation = "run_front"
		elif right:
			$AnimatedSprite.animation = "run_front_flip"
		velocity = velocity.normalized() * speed
		#Down and right, flip front h
		#Up and left, flip back h
	
func _physics_process(delta):
	get_input()
	var collide = move_and_collide(velocity * delta)
	if collide:
		print(collide)
	var tilenode = get_parent().get_node("tilemap")
	var coords = tilenode.world_to_map($goblinfeet.global_position)
	if coords != past_coords:
		past_coords = coords
		print(coords)
	#Use this to print our current coords, only when they update. Our tile is measured as the location of goblin feet (the base of button)


func _on_farm_pause_gobbo(pause):
	paused = pause
