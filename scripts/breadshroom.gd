extends Area2D

signal grown(tile)
signal delete(tile, dead, harvested)

var tile_pos 

var harvested
var composted

var data = {"temp":[0,1,2,3],"light":[0,1,2,3],"moist":[0,1,2,3]}

func _ready():
	#Start the growth timer
	$growtimer.start()
	get_tree().get_root().get_node("farm").connect("death_checker",self,"_on_farm_death_checker")
	connect("delete", get_tree().get_root().get_node("farm"), "_breadshroom_is_delete")
func init(tile):
	#import the tilepos
	tile_pos = tile
	print("Created Breadshroom at: " + str(tile_pos))

func _on_growtimer_timeout():
	#Set it to be grown
	$AnimatedSprite.animation = "grown"
	#Connect the _breadshroom_is_grown function with this signal
	connect("grown", get_tree().get_root().get_node("farm"), "_breadshroom_is_grown")
	#Call it!
	emit_signal("grown", tile_pos)
	$MushroomGrownPlay.play()

func _on_TextureButton_pressed():
	harvested = false
	composted = false
	var mouse_click_pos = get_global_mouse_position()
	$popup/harvestcompost.set_position(mouse_click_pos)
	$popup/harvestcompost.popup()

func _on_farm_death_checker(input_tile, type, values):
	if input_tile == tile_pos:
		var temp = clamp(values.TEMP, 0, 3)
		var light = clamp(values.LIGHT, 0, 3)
		var moisture = clamp(values.MOISTURE, 0, 3)
		var death_check = true
		if data["temp"].has(temp):
			if data["light"].has(light):
				if data["moist"].has(moisture):
					death_check = false
		if death_check == true:
			emit_signal("delete", tile_pos, death_check,"dead")
			queue_free()

func delete(tile_pos,output):
	connect("delete", get_tree().get_root().get_node("farm"), "_breadshroom_is_delete")
	emit_signal("delete", tile_pos, false, output)
	queue_free()


func _on_harvest_pressed():
	harvested = true
	$popup/harvestcompost.hide()
	delete(tile_pos, "harvested")
func _on_compost_pressed():
	composted = true
	$popup/harvestcompost.hide()
	delete(tile_pos, "composted")
