extends Area2D

signal grown(tile)
signal delete(tile, dead, harvested)

var tile_pos 

var harvested
var composted

var data = {"temp":[3],"light":[3],"moist":[3]}

func _ready():
	#Start the growth timer
	$growtimer.start()
	get_tree().get_root().get_node("farm").connect("death_checker",self,"_on_farm_death_checker")
	connect("delete", get_tree().get_root().get_node("farm"), "_poisionshroom_is_delete")

func init(tile):
	#import the tilepos
	tile_pos = tile
	print("Created Poisionshroom at: " + str(tile_pos))

func _on_growtimer_timeout():
	#Set it to be grown
	$AnimatedSprite.animation = "grown"
	#Connect the _breadshroom_is_grown function with this signal
	connect("grown", get_tree().get_root().get_node("farm"), "_poisionshroom_is_grown")
	#Call it!
	emit_signal("grown", tile_pos)
	$MushroomGrownPlay.play()

func _on_click_pressed():
	harvested = false
	composted = false
	var mouse_click_pos = get_global_mouse_position()
	$popup/harvestcompost.set_position(mouse_click_pos)
	$popup/harvestcompost.popup()

func _on_farm_death_checker(input_tile, type, values):
	if input_tile == tile_pos:
		print("Death Check for Poisionshroom at " + str(tile_pos))
		var temp = clamp(values.TEMP, 0, 3)
		var light = clamp(values.LIGHT, 0, 3)
		var moisture = clamp(values.MOISTURE, 0, 3)
		
		print(values)
		
		var death_check = true
		if data["temp"].has(temp) and data["light"].has(light) and data["moist"].has(moisture):
				print("Death Check Passed at " + str(tile_pos))
				death_check = false
		if death_check == true:
			print("Death Check Failed at " + str(tile_pos))
			emit_signal("delete", tile_pos, death_check, "dead")
			queue_free()

func delete(tile_pos,output):
	connect("delete", get_tree().get_root().get_node("farm"), "_poisionshroom_is_delete")
	emit_signal("delete", tile_pos, false, output)
	queue_free()

func _on_harvest_pressed():
	harvested = true
	$popup/harvestcompost.hide()
	delete(tile_pos, "harvested")

func _on_compost_pressed():
	composted = true
	$popup/harvestcompost.hide()
	delete(tile_pos, "composted")
