extends Node2D
var harvest
var compost 
var tiles = []

signal fertiliser_update(num)
signal temp_update(num)
signal light_update(num)
signal moist_update(num)
signal harvert_list_update(input)
signal pause_gobbo(pause)


signal death_checker(tile,type,value)

var tilearray
var fertiliser = 6
var largest
var smallest
var mouse_pos

var selectedtype = ""
var mouse_click_pos

#The dict used by each map tile to store values of the tile
var values = {
	"TYPE": -1,
	"IS_PLANTED": false,
	"IS_PLANTABLE": false,
	"PLANTED": "",
	"TEMP": 0,
	"LIGHT": 0,
	"MOISTURE": 0,
	"HARVESTABLE": false
}

func _ready():
	init_grid()
	default_tile_values()
	pass

func _on_farm_fertiliser_update(num):
	fertiliser += num

func init_grid():
	#Get a tile array of every map cell. this is unsorted and of type vector2
	tilearray = $tilemap.get_used_cells()
	var largestX = 0
	var smallestX = 0
	var largestY = 0
	var smallestY = 0
	#For every value, find the largest X,Y and smallest X,Y 
	for i in tilearray:
		if i.x > largestX:
			largestX = i.x
		if i.x < smallestX:
			smallestX = i.x
		if i.y > largestY:
			largestY = i.y
		if i.y < smallestY:
			smallestY = i.y

	largestX += 1
	largestY += 1
	#off by one hack fix
	
	#Create a 2d array from smallestX to largestX and smallestY to largestY to store our mushroom tile data in
	for x in range(smallestX,largestX):
		tiles.append([])
		for y in range(smallestY,largestY):
			tiles[x].append([])
			tiles[x][y] = values.duplicate()
			tiles[x][y]["TYPE"] = $tilemap.get_cell(x,y)
			#We can now lookup the type by using $tilemap.get_cell or tiles. These need to stay the same
			if tiles[x][y]["TYPE"] == 0:
				tiles[x][y]["IS_PLANTABLE"] = true
			#If its type dirt, thats plantable

func _unhandled_input(event):
	#Check for mouse click AND plantmenu isn't visible AND start button isn't visible
	if event is InputEventMouseButton and $popuplayer/plantmenu.visible == false:
		#If the mouse click was a press and the left button...
		emit_signal("pause_gobbo",true)
		if event.button_index == BUTTON_LEFT and event.pressed:
			#Get mouse pos, convert to tiles and store that, then look up tiles
			mouse_click_pos = get_global_mouse_position()
			mouse_pos = $tilemap.world_to_map(mouse_click_pos)
			var value = tiles[mouse_pos.x][mouse_pos.y]
			#If the tile is plantable and not planted on, open the planting menu
			if value.IS_PLANTABLE == true and value.IS_PLANTED == false:
				$popuplayer/plantmenu.set_position(mouse_click_pos)
				$popuplayer/plantmenu.popup()

	if event is InputEventMouseMotion:
		var past_coords = Vector2(0,0)
		var coords = $tilemap.world_to_map(get_global_mouse_position())
		var value = tiles[coords.x][coords.y]
		emit_signal("temp_update",value.TEMP)
		emit_signal("light_update",value.LIGHT)
		emit_signal("moist_update",value.MOISTURE)

func plant_tile(plant_type):
	#Load the types of mushroom here
	var breadshroom = load("res://scripts/breadshroom.tscn")
	var thermoshroom = load("res://scripts/thermoshroom.tscn")
	var lichenplant = load("res://scripts/lichen.tscn")
	var dankshroom = load("res://scripts/dankshroom.tscn")
	var poisionshroom = load("res://scripts/poisionshroom.tscn")
	#set values for shorthand of tiles lookup
	var value = tiles[mouse_pos.x][mouse_pos.y]
	#Check its plantable and not planted on
	if value.IS_PLANTABLE == true and value.IS_PLANTED == false and fertiliser > 0:
		#Get the location
		var check = false
		if plant_type == "breadshroom":
			check = tile_checker(value,"breadshroom")
			if check == true:
				value.IS_PLANTED = true
				emit_signal("fertiliser_update", -1)
				#Create an instance
				var bread = breadshroom.instance()
				#Pass the tiles to it (we need this later)
				bread.init(mouse_pos)
				#Set the pos to LOC (last mouse press)
				bread.position = mouse_click_pos
				#Create it as a child of shroomnode
				$shroomnode.add_child(bread)
				#Assign values in our values array
				value.PLANTED = "breadshroom"

		if plant_type == "thermoshroom":
			check = tile_checker(value,"thermoshroom")
			if check == true:
				value.IS_PLANTED = true
				emit_signal("fertiliser_update", -1)
				var thermo = thermoshroom.instance()
				thermo.init(mouse_pos)
				thermo.position = mouse_click_pos
				$shroomnode.add_child(thermo)
				value.PLANTED = "thermoshroom"
			
		if plant_type == "lichen":
			check = tile_checker(value,"lichen")
			if check == true:
				value.IS_PLANTED = true
				emit_signal("fertiliser_update", -1)
				var lichen = lichenplant.instance()
				lichen.init(mouse_pos)
				lichen.position = mouse_click_pos
				$shroomnode.add_child(lichen)
				value.PLANTED = "lichen"
			
		if plant_type == "dank":
			check = tile_checker(value,"dank")
			if check == true:
				value.IS_PLANTED = true
				emit_signal("fertiliser_update", -1)
				var dank = dankshroom.instance()
				dank.init(mouse_pos)
				dank.position = mouse_click_pos
				$shroomnode.add_child(dank)
				value.PLANTED = "dank"

		if plant_type == "poison":
			check = tile_checker(value,"poisonshroom")
			if check == true:
				value.IS_PLANTED = true
				emit_signal("fertiliser_update", -1)
				var posion = poisionshroom.instance()
				posion.init(mouse_pos)
				posion.position = mouse_click_pos
				$shroomnode.add_child(posion)
				value.PLANTED = "poison"

func raise_tile_value(tile,type):
#	tile = tilepos
#	type = heat, moist, light
#	value = starting values (2 goes down to 1) 0 reset to 0 in range
	var value = tiles[tile.x][tile.y]
	var original_value = value[type]
	for x in range(tile.x-2,tile.x+3):
		for y in range(tile.y-2,tile.y+3):
			if tiles[x][y][type] < 37:
				if x == tile.x and y == tile.y:
					pass
				else:
					tiles[x][y][type] += 1
				death_checker(Vector2(x,y),type,value)

	for x in range(tile.x-1,tile.x+2):
		for y in range(tile.y-1,tile.y+2):
			if tiles[x][y][type] < 37:
				if x == tile.x and y == tile.y:
					pass
				else:
					tiles[x][y][type] += 1
				death_checker(Vector2(x,y),type,value)
#	for i in range(value-1):
#		for x in range(tile.x-i,tile.x+i):
#			for y in range(tile.y-i,tile.y+i):
#				tiles[x][y].TEMP += 1
#				print(tiles[tile.x][tile.y].TEMP)
	
func lower_tile_value(tile,type):
#	tile = tilepos
#	type = heat, moist, light
#	value = starting values (2 goes down to 1) 0 reset to 0 in range
	var value = tiles[tile.x][tile.y]
	for x in range(tile.x-2,tile.x+3):
		for y in range(tile.y-2,tile.y+3):
			if tiles[x][y][type] > 0:
				if x == tile.x and y == tile.y:
					pass
				else:
					tiles[x][y][type] -= 1
				death_checker(Vector2(x,y),type,value)
				
	for x in range(tile.x-1,tile.x+2):
		for y in range(tile.y-1,tile.y+2):
			if tiles[x][y][type] > 0:
				if x == tile.x and y == tile.y:
					pass
				else:
					tiles[x][y][type] -= 1
				death_checker(Vector2(x,y),type,value)

	

func death_checker(tile,type,value):
	emit_signal("death_checker",tile,type,value)
	
func tile_checker(value,type):
	var data
	var breadshroom_safe_data = {"temp":[0,1,2,3],"light":[0,1,2,3],"moist":[0,1,2,3]}

	var lichen_safe_data = {"temp":[2,3],"light":[0,1],"moist":[0,1,2,3]}

	var thermoshroom_safe_data = {"temp":[2,3],"light":[1,2,3],"moist":[0,1]}

	var dankshroom_safe_data = {"temp":[0,1],"light":[0,2],"moist":[2,3]}

	var posion_safe_data = {"temp":[3],"light":[3],"moist":[3]}

	if type == "breadshroom":
		data = breadshroom_safe_data
	elif type == "lichen":
		data = lichen_safe_data
	elif type == "thermoshroom":
		data = thermoshroom_safe_data
	elif type == "dank":
		data = dankshroom_safe_data
	elif type == "poisonshroom":
		data = "poisonshroom"
	else:
		print("this shouldn't occur...")
		
	var temp = clamp(value.TEMP, 0, 3)
	var light = clamp(value.LIGHT, 0, 3)
	var moisture = clamp(value.MOISTURE, 0, 3)
	if data["temp"].has(temp):
		if data["light"].has(light):
			if data["moist"].has(moisture):
				print("we can plant")
				return true
	return false

func default_tile_values():
	var heat2 = [Vector2(15,15), Vector2(16,15)]
	var heat1 = [Vector2(16,14), Vector2(15,14), Vector2(14,15), Vector2(14,14)]

	var water3 = [Vector2(9,10)]
	var water2 = [Vector2(9,11),Vector2(10,11)]
	
	var light2 = [Vector2(12,15),Vector2(13,15)]
	var light1 = [Vector2(14,15),Vector2(14,14),Vector2(13,14),Vector2(12,14),Vector2(11,14),Vector2(11,15)]
	
	for i in heat2:
		tiles[i.x][i.y]["TEMP"] = 3
	for i in heat1:
		tiles[i.x][i.y]["TEMP"] = 2
	for i in water3:
		tiles[i.x][i.y]["MOISTURE"] = 3
	for i in water2:
		tiles[i.x][i.y]["MOISTURE"] = 2
	for i in light2:
		tiles[i.x][i.y]["LIGHT"] = 3
	for i in light1:
		tiles[i.x][i.y]["LIGHT"] = 2
#======
#Breadshroom

func _breadshroom_is_grown(tile_pos):
	#Once grown call it, set the values correctly
	print("Breadshroom Grown at " + str(tile_pos))
	var value = tiles[tile_pos.x][tile_pos.y]
	value.HARVESTABLE = true
	
func _breadshroom_is_delete(tile_pos,dead,input):
	var value = tiles[tile_pos.x][tile_pos.y]
	if dead == false:
		print("Breadshroom " + input +" at " + str(tile_pos))
		if input == "composted":
			if value.HARVESTABLE == true:
				emit_signal("fertiliser_update", 2)
			else:
				emit_signal("fertiliser_update", 1)
		if input == "harvested":
			if value.HARVESTABLE == true:
				emit_signal("harvert_list_update","Cavern Bread")
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		#Send signal to the HUD to update the fert count in the UI
	if dead == true:
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		
func _on_breadshroom_pressed():
	$popuplayer/plantmenu.hide()
	emit_signal("pause_gobbo",false)
	plant_tile("breadshroom")
	
#======
#Thermoshroom

func _thermoshroom_is_grown(tile_pos):
	#Once grown call it, set the values correctly
	print("Thermoshroom Grown at " + str(tile_pos))
	var value = tiles[tile_pos.x][tile_pos.y]
	value.HARVESTABLE = true
	raise_tile_value(tile_pos,"TEMP")
	
func _thermoshroom_is_delete(tile_pos,dead,input):
	var value = tiles[tile_pos.x][tile_pos.y]
	if dead == false:
		print("Thermoshroom " + input +" at " + str(tile_pos))
		if input == "composted":
			if value.HARVESTABLE == true:
				emit_signal("fertiliser_update", 1)
			else:
				emit_signal("fertiliser_update", 1)
		if input == "harvested":
			if value.HARVESTABLE == true:
				emit_signal("harvert_list_update","Tinderbox Morel")
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		#Send signal to the HUD to update the fert count in the UI
	if dead == true:
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		
func _on_thermoshroom_pressed():
	emit_signal("pause_gobbo",false)
	$popuplayer/plantmenu.hide()
	plant_tile("thermoshroom")

#=====
#Lichen

func _lichen_is_grown(tile_pos):
	#Once grown call it, set the values correctly
	print("Lichen Grown at " + str(tile_pos))
	var value = tiles[tile_pos.x][tile_pos.y]
	value.HARVESTABLE = true
	raise_tile_value(tile_pos,"LIGHT")
	
func _lichen_is_delete(tile_pos,dead,input):
	var value = tiles[tile_pos.x][tile_pos.y]
	if dead == false:
		print("Lichen " + input +" at " + str(tile_pos))
		if input == "composted":
			if value.HARVESTABLE == true:
				emit_signal("fertiliser_update", 1)
			else:
				emit_signal("fertiliser_update", 1)
		if input == "harvested":
			if value.HARVESTABLE == true:
				emit_signal("harvert_list_update","Luminous Lichen")
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		#Send signal to the HUD to update the fert count in the UI
	if dead == true:
		value.HARVESTABLE = false
		value.IS_PLANTED = false

func _on_lichen_pressed():
	emit_signal("pause_gobbo",false)
	$popuplayer/plantmenu.hide()
	plant_tile("lichen")
#=====
#dankshroom

func _dankshroom_is_grown(tile_pos):
	#Once grown call it, set the values correctly
	print("Dankshroom Grown at " + str(tile_pos))
	var value = tiles[tile_pos.x][tile_pos.y]
	value.HARVESTABLE = true
	raise_tile_value(tile_pos,"MOISTURE")
	
func _dankshroom_is_delete(tile_pos,dead,input):
	var value = tiles[tile_pos.x][tile_pos.y]
	if dead == false:
		print("Dankshroom " + input +" at " + str(tile_pos))
		if input == "composted":
			if value.HARVESTABLE == true:
				emit_signal("fertiliser_update", 1)
			else:
				emit_signal("fertiliser_update", 1)
		if input == "harvested":
			if value.HARVESTABLE == true:
				emit_signal("harvert_list_update","Dankshroom")
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		#Send signal to the HUD to update the fert count in the UI
	if dead == true:
		value.HARVESTABLE = false
		value.IS_PLANTED = false

		
func _on_dankshroom_pressed():
	emit_signal("pause_gobbo",false)
	$popuplayer/plantmenu.hide()
	plant_tile("dank")

#=====
#dankshroom

func _poisonshroom_is_grown(tile_pos):
	#Once grown call it, set the values correctly
	print("Poison Grown at " + str(tile_pos))
	var value = tiles[tile_pos.x][tile_pos.y]
	value.HARVESTABLE = true
#	raise_tile_value(tile_pos,"MOISTURE")
	
func _poisonshroom_is_delete(tile_pos,dead,input):
	var value = tiles[tile_pos.x][tile_pos.y]
	if dead == false:
		print("Poisonshroom " + input +" at " + str(tile_pos))
		if input == "composted":
			if value.HARVESTABLE == true:
				emit_signal("fertiliser_update", 1)
			else:
				emit_signal("fertiliser_update", 1)
		if input == "harvested":
			if value.HARVESTABLE == true:
				emit_signal("harvert_list_update","Funeral Knell")
		value.HARVESTABLE = false
		value.IS_PLANTED = false
		#Send signal to the HUD to update the fert count in the UI
	if dead == true:
		value.HARVESTABLE = false
		value.IS_PLANTED = false

func _on_poisonshroom_pressed():
	emit_signal("pause_gobbo",false)
	$popuplayer/plantmenu.hide()
	plant_tile("dank")
